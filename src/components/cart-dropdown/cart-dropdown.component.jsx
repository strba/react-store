import React from 'react';
import {connect} from 'react-redux';
import {createStructuredSelector} from 'reselect';
import {withRouter} from 'react-router-dom';

import CustomButton from '../custom-button/custom-button.component';
import CartItem from '../cart-item/cart-item.component';
import {selectCartItems} from '../../redux/cart/cart.selectors';
import {toggleCartHidden} from '../../redux/cart/cart.actions.js';

import {makeStyles} from '@material-ui/core/styles';

const useStyles = makeStyles({
    cart_dropdown: {
        position: 'absolute',
        width: '240px',
        height: '340px',
        display: 'flex',
        flexDirection: 'column',
        padding: '20px',
        border: '1px solid black',
        backgroundColor: 'white',
        top: '90px',
        right: '40px',
        zIndex: '5',
        "& .empty-message": {
            fontSize: '18px',
            margin: '50px auto',
        },
        "& .cart-items": {
            height: '240px',
            display: 'flex',
            flexDirection: 'column',
            overflow: 'scroll',
        },

        '& button': {
            marginTop: 'auto',
        }
    }
});


const CartDropdown = ({cartItems, history, dispatch}) => {

    const classes = useStyles();

    return (
        <div className={classes.cart_dropdown}>
            <div className='cart-items'>
                {cartItems.length ? (
                    cartItems.map(cartItem => (
                        <CartItem key={cartItem.id} item={cartItem}/>
                    ))
                ) : (
                    <span className='empty-message'>Your cart is empty</span>
                )}
            </div>
            <CustomButton
                onClick={() => {
                    history.push('/checkout');
                    dispatch(toggleCartHidden());
                }}
            >
                GO TO CHECKOUT
            </CustomButton>
        </div>
    )
};

const mapStateToProps = createStructuredSelector({
    cartItems: selectCartItems
});

export default withRouter(connect(mapStateToProps)(CartDropdown));