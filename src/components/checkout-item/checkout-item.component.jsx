import React from 'react';
import {connect} from 'react-redux';
import {makeStyles} from '@material-ui/core/styles';

import {
    clearItemFromCart,
    addItem,
    removeItem
} from '../../redux/cart/cart.actions';


const useStyles = makeStyles({
    checkout_item: {
        width: '100%',
        display: 'flex',
        minHeight: '100px',
        borderBottom: '1px solid darkgrey',
        padding: '15px 0',
        fontSize: '20px',
        alignItems: 'center',
        '& .image-container': {
            width: '23%',
            paddingRight: '15px',

            '& img': {
                width: '100%',
                height: '100%',
            }
        },
        '& .name, .quantity, .price': {
            width: '23%',
        },

        '& .quantity': {
            display: 'flex',

            '& .arrow': {
                cursor: 'pointer',
            },

            '& .value': {
                margin: '0 10px',
            }
        },

        '& .remove-button': {
            paddingLeft: '12px',
            cursor: 'pointer',
        }
    }
});

const CheckoutItem = ({cartItem, clearItem, addItem, removeItem}) => {
    const classes = useStyles();

    const {name, imageUrl, price, quantity} = cartItem;
    return (
        <div className={classes.checkout_item}>
            <div className='image-container'>
                <img src={imageUrl} alt='item'/>
            </div>
            <span className='name'>{name}</span>
            <span className='quantity'>
        <div className='arrow' onClick={() => removeItem(cartItem)}>
          &#10094;
        </div>
        <span className='value'>{quantity}</span>
        <div className='arrow' onClick={() => addItem(cartItem)}>
          &#10095;
        </div>
      </span>
            <span className='price'>{price}</span>
            <div className='remove-button' onClick={() => clearItem(cartItem)}>
                &#10005;
            </div>
        </div>
    );
};

const mapDispatchToProps = dispatch => ({
    clearItem: item => dispatch(clearItemFromCart(item)),
    addItem: item => dispatch(addItem(item)),
    removeItem: item => dispatch(removeItem(item))
});

export default connect(
    null,
    mapDispatchToProps
)(CheckoutItem);