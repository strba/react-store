import React from 'react';
import {connect} from 'react-redux';

import CustomButton from '../custom-button/custom-button.component';
import {addItem} from '../../redux/cart/cart.actions';

import {makeStyles} from '@material-ui/core/styles';

const useStyles = makeStyles({
    collection_item: {
        width: '22vw',
        display: 'flex',
        flexDirection: 'column',
        height: '350px',
        alignItems: 'center',
        position: 'relative',
        '& .image': {
            width: '100%',
            height: '95%',
            backgroundSize: 'cover',
            backgroundPosition: 'center',
            marginBottom: '5px',
        },
        '& button': {
            width: '80%',
            opacity: '0.7',
            position: 'absolute',
            top: '255px',
            display: 'none',
        },
        '&:hover': {
            '& button': {
                opacity: 0.85,
                display: 'flex'
            },
            '& .image':{
                opacity: 0.8,
            }
        }
    },
    collection_footer: {
        width: '100%',
        height: '5%',
        display: 'flex',
        justifyContent: 'space-between',
        fontSize: '18px',


    },
    name: {
        width: '90%',
        marginBottom: '15px',
    },
    price: {
        width: '10%',
    },
});

const CollectionItem = ({item, addItem}) => {
    const classes = useStyles();
    const {name, price, imageUrl} = item;

    return (
        <div className={classes.collection_item}>
            <div
                className="image"
                style={{
                    backgroundImage: `url(${imageUrl})`
                }}
            />
            <div className={classes.collection_footer}>
                <span className={classes.name}>{name}</span>
                <span className={classes.price}>{price}</span>
            </div>
            <CustomButton onClick={() => addItem(item)} inverted='true'>
                Add to cart
            </CustomButton>
        </div>
    );
};

const mapDispatchToProps = dispatch => ({
    addItem: item => dispatch(addItem(item))
});

export default connect(
    null,
    mapDispatchToProps
)(CollectionItem);
