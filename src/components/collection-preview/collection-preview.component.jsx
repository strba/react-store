import React from 'react';

import CollectionItem from '../collection-item/collection-item.component';

import {makeStyles} from '@material-ui/core/styles';

const useStyles = makeStyles({
    collection_preview: {
    display: 'flex',
    flexDirection: 'column',
    marginBottom: '30px',

    '& .title': {
        fontSize: '28px',
        marginBottom: '25px',
    },

    '& .preview': {
        display: 'flex',
        justifyContent: 'space-between',
    }
}

});

const CollectionPreviewComponent = ({ title, items }) => {
    const classes = useStyles();

    return(
    <div className={classes.collection_preview}>
        <h1 className='title'>{title.toUpperCase()}</h1>
        <div className='preview'>
            {items
                .filter((item, idx) => idx < 4)
                .map(item => (
                    <CollectionItem key={item.id} item={item} />
                ))}
        </div>
    </div>
)};

export default CollectionPreviewComponent;