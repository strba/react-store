import React from 'react';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';

import CollectionPreview from '../collection-preview/collection-preview.component';

import { selectCollectionsForPreview } from '../../redux/shop/shop.selectors';

import {makeStyles} from '@material-ui/core/styles';

const useStyles = makeStyles({
    collections_overview: {
    display: 'flex',
    flexDirection: 'column'
}

});


const CollectionsOverview = ({ collections }) => {
    const classes = useStyles();

    return(
    <div className={classes.collections_overview}>
        {collections.map(({ id, ...otherCollectionProps }) => (
            <CollectionPreview key={id} {...otherCollectionProps} />
        ))}
    </div>
)};

const mapStateToProps = createStructuredSelector({
    collections: selectCollectionsForPreview
});

export default connect(mapStateToProps)(CollectionsOverview);