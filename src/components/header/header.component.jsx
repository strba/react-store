import React from 'react';
import {Link} from 'react-router-dom';
import {connect} from 'react-redux';
import {createStructuredSelector} from "reselect";
import {auth} from '../../firebase/firebase.utils';
import CartIcon from '../cart-icon/cart-icon.component';
import CartDropdown from '../cart-dropdown/cart-dropdown.component';
import {selectCartHidden} from "../../redux/cart/cart.selectors";
import {selectCurrentUser} from "../../redux/user/user.selectors";
import {ReactComponent as Logo} from '../../assets/crown.svg';
import {makeStyles} from '@material-ui/core/styles';

const useStyles = makeStyles({
    header: {
        height: '70px',
        width: '100%',
        display: 'flex',
        justifyContent: 'space-between',
        marginBottom: '25px',
    },
    logo_container: {
        height: '100%',
        width: '70px',
        padding: '25px'
    },
    options: {
        width: '50%',
        height: '100%',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'flex-end',
        textTransform: 'uppercase',
    },
    option: {
        padding: '10px 15px',
        cursor: 'pointer'
    }
});


const Header = ({currentUser, hidden}) =>{
    const classes = useStyles();
    return(
    <div className={classes.header}>
        <Link className={classes.logo_container} to='/'>
            <Logo className='logo'/>
        </Link>
        <div className={classes.options}>
            <Link className={classes.option} to='/shop'>
                SHOP
            </Link>
            <Link className={classes.option} to='/shop'>
                CONTACT
            </Link>
            {currentUser ? (
                <div className={classes.option} onClick={() => auth.signOut()}>
                    SIGN OUT
                </div>
            ) : (
                <Link className={classes.option} to='/signin'>
                    SIGN IN
                </Link>
            )}
            <CartIcon/>
        </div>
        {hidden ? null : <CartDropdown/>}
    </div>
)};

const mapStateToProps = createStructuredSelector({
    currentUser: selectCurrentUser,
    hidden: selectCartHidden
});

export default connect(mapStateToProps)(Header);
