import React from 'react';
import {withRouter} from 'react-router-dom'

import {makeStyles} from '@material-ui/core/styles';

const useStyles = makeStyles({
    menu_item: {
        minWidth: '30%',
        height: '240px',
        flex: '1 1 auto',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        border: '1px solid black',
        margin: '0 7.5px 15px',
        overflow: 'hidden',

        '&:hover': {
            cursor: 'pointer',

            '& .background-image': {
                transform: 'scale(1.1)',
                transition: 'transform 6s cubic-bezier(0.25, 0.45, 0.45, 0.95)',
            },

            '& .content': {
                opacity: 0.9,
            }
        },
        '&:first-child': {
            marginRight: '7.5px',
        },

        '&:last-child': {
            marginLeft: '7.5px',
        },
        '& .background-image': {
            width: '100%',
            height: '100%',
            backgroundSize: 'cover',
            backgroundPosition: 'center',
        },
        '& .content': {
            height: '90px',
            padding: '0 25px',
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
            justifyContent: 'center',
            border: '1px solid black',
            backgroundColor: 'white',
            opacity: 0.7,
            position: 'absolute',

            '& .title': {
                fontWeight: 'bold',
                marginBottom: '6px',
                fontSize: '22px',
                color: '#4a4a4a',
            },

            '& .subtitle': {
                fontWeight: 'lighter',
                fontSize: '16px',
            }
        }
    },
    large: {
        height: '380px',
    },


});
const MenuItem = ({title, imageUrl, size, history, linkUrl, match}) => {
    const classes = useStyles();
    return (
        <div className={`${classes.menu_item} ${size? classes.large : ''}`} onClick={() => history.push(`${match.url}${linkUrl}`)}>
            <div className='background-image' style={{
                backgroundImage: `url(${imageUrl})`
            }}/>
            <div className='content'>
                <h1 className='title'>{title.toUpperCase()}</h1>
                <span className='subtitle'>SHOP NOW</span>
            </div>
        </div>)
}

export default withRouter(MenuItem)
