import React from "react";
import StripeCheckout from "react-stripe-checkout"

const StripeCheckoutButton = ({price}) => {
    const priceForStripe = price * 100;
    const publishableKey = 'pk_test_51I48J6C5HNLDBEFzJG5iW7yUNylaXs3tKKDK4Zn3etThDAnPyNhVjxApvWSRkeabMQwk6ISWetJt6gkNhOdAG3co009ERs8K08'
const onToken = token =>{
        console.log(token)
        alert('Susses payment')
}

    return(
    <StripeCheckout
        label='Pay Now'
        name='React Store'
        billingAddress
        shippingAddress
        description={`Your total is $${price}`}
        amount={priceForStripe}
        panelLabel='Pay Now'
        token={onToken}
        stripeKey={publishableKey}
    />
)
}

export default StripeCheckoutButton