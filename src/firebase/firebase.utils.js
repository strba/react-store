import firebase from 'firebase/app'
import 'firebase/firestore'
import 'firebase/auth'

const config = {
    apiKey: "AIzaSyBxlMKiWjtBNmGTMVfY16ay1lAlA0XXNAg",
    authDomain: "store-db-ecc77.firebaseapp.com",
    projectId: "store-db-ecc77",
    storageBucket: "store-db-ecc77.appspot.com",
    messagingSenderId: "460599364360",
    appId: "1:460599364360:web:c23339a1bac4c24bf83abd",
    measurementId: "G-Y042BN1DZ1"
};

firebase.initializeApp(config);

export const createUserProfileDocument = async (userAuth, additionalData) => {
    if (!userAuth) return;

    const userRef = firestore.doc(`users/${userAuth.uid}`);

    const snapShot = await userRef.get();

    if (!snapShot.exists) {
        const { displayName, email } = userAuth;
        const createdAt = new Date();
        try {
            await userRef.set({
                displayName,
                email,
                createdAt,
                ...additionalData
            });
        } catch (error) {
            console.log('error creating user', error.message);
        }
    }

    return userRef;
};

export const addCollectionAndDocuments = async (collectionKey, objectToAdd) => {
    const collectionRef = firestore.collection(collectionKey);

    const batch = firestore.batch();

    objectToAdd.forEach(obj =>{
        const newDocRef = collectionRef.doc();
        batch.set(newDocRef, obj)
    })

    return await batch.commit()

}


export const convertCollectionsSnapshotToMap = (collections) =>{
    const transformedCollection = collections.docs.map(
        doc => {
            const {title, items} = doc.data();
            return{
                routeName: encodeURI(title.toLowerCase()),
                id: doc.id,
                title,
                items
            }
        }

    )
    return transformedCollection.reduce((accumulator, collection) => {
        accumulator[collection.title.toLowerCase()] = collection;
        return accumulator
    },{})
}

export const auth = firebase.auth();
export const firestore = firebase.firestore();

const provider = new firebase.auth.GoogleAuthProvider();
provider.setCustomParameters({ prompt: 'select_account' });
export const signInWithGoogle = () => auth.signInWithPopup(provider);

export default firebase;
