import React from 'react';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import StripeCheckoutButton from "../../components/stripe-button/stripe-button.component";

import CheckoutItem from '../../components/checkout-item/checkout-item.component';

import {
    selectCartItems,
    selectCartTotal
} from '../../redux/cart/cart.selectors';

import {makeStyles} from '@material-ui/core/styles';


const useStyles = makeStyles({
    checkout_page: {
    width: '55%',
    minHeight: '90vh',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    margin: '50px auto 0',

'& .checkout-header': {
        width: '100%',
        padding: '10px 0',
        display: 'flex',
        justifyContent: 'space-between',
        borderBottom: '1px solid darkgrey',

    '& .header-block': {
            textTransform: 'capitalize',
            width: '23%',

        '&:last-child': {
                width: '8%',
            }
        }
    },

'& .total': {
        marginTop: '30px',
        marginLeft: 'auto',
        fontSize: '36px',
    },

    '& button': {
        marginLeft: 'auto',
        marginTop: '50px',
    },

'& .test-warning': {
        textAlign: 'center',
        marginTop: '40px',
        fontSize: '24px',
        color: 'red',
    }
}

});
const CheckoutPage = ({ cartItems, total }) =>{
    const classes = useStyles();

    return(
    <div className={classes.checkout_page}>
        <div className='checkout-header'>
            <div className='header-block'>
                <span>Product</span>
            </div>
            <div className='header-block'>
                <span>Description</span>
            </div>
            <div className='header-block'>
                <span>Quantity</span>
            </div>
            <div className='header-block'>
                <span>Price</span>
            </div>
            <div className='header-block'>
                <span>Remove</span>
            </div>
        </div>
        {cartItems.map(cartItem => (
            <CheckoutItem key={cartItem.id} cartItem={cartItem} />
        ))}
        <div className='total'>TOTAL: ${total}</div>
        <div className='test-warning'>
            *Please use the following test credit cart for payments*
            <br/>
            4242 4242 4242 4242 - Exp: 01/22 - CVV: 123
        </div>
        <StripeCheckoutButton price={total}/>
    </div>
)};

const mapStateToProps = createStructuredSelector({
    cartItems: selectCartItems,
    total: selectCartTotal
});

export default connect(mapStateToProps)(CheckoutPage);