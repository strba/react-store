import React from 'react';
import {connect} from 'react-redux';

import CollectionItem from '../../components/collection-item/collection-item.component';

import {selectCollection} from '../../redux/shop/shop.selectors';

import {makeStyles} from '@material-ui/core/styles';

const useStyles = makeStyles({
    collection_page: {
        display: 'flex',
        flexDirection: 'column',

        '& .title': {
            fontSize: '38px',
            margin: '0 auto 30px'
        },

        '& .items': {
            display: 'grid',
            gridTemplateColumns: '1fr 1fr 1fr 1fr',
            gridGap: '10px',

            '& > div': {
                marginBottom: '30px'
            }
        }
    }
});

const CollectionPage = ({collection}) => {
    const {title, items} = collection;
    const classes = useStyles();

    return (
        <div className={classes.collection_page}>
            <h2 className='title'>{title}</h2>
            <div className='items'>
                {items.map(item => (
                    <CollectionItem key={item.id} item={item}/>
                ))}
            </div>
        </div>
    );
};

const mapStateToProps = (state, ownProps) => ({
    collection: selectCollection(ownProps.match.params.collectionId)(state)
});

export default connect(mapStateToProps)(CollectionPage);