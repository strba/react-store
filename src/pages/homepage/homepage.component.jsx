import React from 'react';
import Directory from "../../components/directory/directory.componenet";
import { makeStyles } from '@material-ui/core/styles';


const useStyles = makeStyles({
    root: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center'
    },
});


const HomePage = () =>{
    const classes = useStyles();
    return(
    <div className={classes.root}>
        <Directory/>
    </div>)
}

export default HomePage