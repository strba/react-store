import React from 'react';

import SignIn from '../../components/sign-in/sign-in.component';
import SignUp from '../../components/sign-up/sign-up.component';

import {makeStyles} from '@material-ui/core/styles';

const useStyles = makeStyles({
    sign_in_and_sign_up: {
    width: '850px',
    display: 'flex',
    justifyContent: 'space-between',
    margin: '30px auto'
}

});
const SignInAndSignUpPage = () =>{
    const classes = useStyles();
    return(
    <div className={classes.sign_in_and_sign_up}>
        <SignIn />
        <SignUp />
    </div>
)};

export default SignInAndSignUpPage;